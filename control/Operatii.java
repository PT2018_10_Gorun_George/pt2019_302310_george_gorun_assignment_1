package control;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import data.Polinom;
import data.Monom;

public class Operatii {
	public Polinom sumPolinom(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		ArrayList<Monom> pol1 = p1.getMonoms();
		ArrayList<Monom> pol2 = p2.getMonoms();
		ArrayList<Monom> used1 = new ArrayList<>();
		ArrayList<Monom> used2 = new ArrayList<>();
		for (Monom a : pol1)
			for (Monom b : pol2) {
				if (a.getGrad() == b.getGrad()) {
					result.addMonom(new Monom(a.getCoef() + b.getCoef(), a.getGrad()));
					used1.add(a);
					used2.add(b);
				}
			}
		for (Monom m : pol1)
			if (!used1.contains(m))
				result.addMonom(m);
		for (Monom n : pol2)
			if (!used2.contains(n))
				result.addMonom(n);

		return result;
	}

	public Polinom difPolinom(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		ArrayList<Monom> pol1 = p1.getMonoms();
		ArrayList<Monom> pol2 = p2.getMonoms();
		ArrayList<Monom> used1 = new ArrayList<>();
		ArrayList<Monom> used2 = new ArrayList<>();
		for (Monom a : pol1)
			for (Monom b : pol2) {
				if (a.getGrad() == b.getGrad()) {
					result.addMonom(new Monom(a.getCoef() - b.getCoef(), a.getGrad()));
					used1.add(a);
					used2.add(b);
				}
			}
		for (Monom m : pol1)
			if (!used1.contains(m))
				result.addMonom(m);
		for (Monom n : pol2)
			if (!used2.contains(n))
				result.addMonom(new Monom(-(n.getCoef()),n.getGrad()));

		return result;

	}

	public Polinom mulPolinom(Polinom p1, Polinom p2) {
		Polinom result = new Polinom();
		ArrayList<Monom> poli1 = p1.getMonoms();
		ArrayList<Monom> poli2 = p2.getMonoms();
		for (Monom m : poli1) {
			for (Monom n : poli2)
				result.addMonom(new Monom(n.getCoef() * m.getCoef(), n.getGrad() * m.getGrad()));
		}
		return result;

	}

	public Polinom derivPolinom(Polinom p1) {
		Polinom result = new Polinom();
		ArrayList<Monom> poli = p1.getMonoms();
		for (Monom m : poli)
			if (m.getGrad() == 0) {
				result.addMonom(new Monom(0, 0));
			} else
				result.addMonom(new Monom(m.getCoef() * m.getGrad(), m.getGrad() - 1));
		return result;
	}

	public Polinom integrPolinom(Polinom p1) {
		Polinom result = new Polinom();
		ArrayList<Monom> poli = p1.getMonoms();
		for (Monom m : poli)
			result.addMonom(new Monom(m.getCoef() / (m.getGrad() + 1), m.getGrad() + 1));
		return result;

	}
	public Polinom ordonare(Polinom p1)
	{Polinom result=new Polinom();
	
	ArrayList<Monom> poli1=p1.getMonoms();
	//Collections.sort(poli1,poli1.get(0).getGrad());	
	
return result;
}
}