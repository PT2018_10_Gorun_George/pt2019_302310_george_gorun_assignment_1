package data;

public class Monom {

	private float coef;
	private int grad;
	public Monom() {}

	public Monom(float coef, int grad) {
		this.grad = grad;
		this.coef = coef;
	}

	public float getCoef() {
		return coef;
	}

	public int getGrad() {
		return grad;
	}

	public void setCoef(float coef) {
		this.coef = coef;
	}

	public void setGrad(int grad) {
		this.grad = grad;
	}

	public String firstMonom() {
		String string = new String();

		string = string.concat(coef + "X^");

		if (grad < 0)
			string = string.concat("(" + grad + ")");
		else
			string = string.concat(Integer.toString(grad));
		return string;
	}

	public String toString() {
		String string = new String();
		if (coef < 0)
			string = string.concat(Float.toString(coef));
		else if(coef < 0 && coef != -1)
			string=string+ " "+ coef + "X";
		else if (coef == 0.0)
			string = string.concat("");
		else
			string = string.concat("+" + coef);

		if (grad < 0)
			string = string.concat("X^" + "(" + grad + ")");
		else if (grad == 0)
			return string;
		else
			string = string.concat("X^" + grad);
		return string;
	}
}
