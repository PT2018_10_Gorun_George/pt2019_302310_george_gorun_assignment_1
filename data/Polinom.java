package data;
import control.ParsePolinom;
import java.util.ArrayList;
public class Polinom {
ArrayList<Monom> polinom;

	public Polinom() {
		polinom=new ArrayList<>();
		
	}
	public Polinom(String s)
	{ParsePolinom parser = new ParsePolinom();
		ArrayList<Monom> polynom1 = parser.parsePolinom(s);
	for(Monom m: polynom1)
		polynom1.add(m);
		polinom=polynom1; 
	
		
	}
	  public void addMonom(Monom monom){
	        polinom.add(monom);
	  }
	  public ArrayList<Monom> getMonoms() {
	        return polinom;
	    }

	  public String toString() {
	        if (polinom.size() > 0) {
	            String string = new String();
	            string = string.concat(polinom.get(0).firstMonom());
	            if (polinom.size() > 1) {
	                for (int i = 1; i < polinom.size(); i++) {
	                    string = string.concat(polinom.get(i).toString());
	                }
	            }
	            return string;
	        }
	        return new String("nu contine monoame");
	    }
	
}
