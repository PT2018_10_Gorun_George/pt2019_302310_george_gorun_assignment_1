package Main;
import data.Polinom;
import control.ParsePolinom;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import data.Monom;
import control.Operatii;
import data.Polinom;
import control.ParsePolinom;

public class GUI extends JFrame {
	// declaram elementele
	private JFrame frame;

	private JButton adunare;
	private JButton scadere;
	private JButton inmultire;
	private JButton derivare;
	private JButton integrare;
	private JButton reset;
	private JLabel titlu;
	private JLabel Rezultat;
	private JTextField Polinom1;
	private JTextField Polinom2;
	private JTextField RezultatAfisat;
	private Polinom polinom1;
	private Polinom polinom2;
	private Operatii opManager;
	private ParsePolinom parse;

	private String poly1;
	private String poly2;

	public GUI() {
		polinom1 = new Polinom();
		polinom2 = new Polinom();

		frame = new JFrame("CalculatorPolinoame");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(800, 800);
		adunare = new JButton("Adunare");
		scadere = new JButton("Scadere");
		inmultire = new JButton("Inmultire");
		derivare = new JButton("Derivare");
		integrare = new JButton("Integrare");
		reset=new JButton("Reset");
		frame.add(adunare);
		frame.add(scadere);
		frame.add(inmultire);
		frame.add(derivare);
		frame.add(integrare);
		frame.add(reset);

		Rezultat = new JLabel("Rezultat: ");
		titlu = new JLabel("Calculator: pentru polinoame care contin un coeficient cu - se va scrie +-X^grad");

		frame.add(Rezultat);
		frame.add(titlu);

		Polinom1 = new JTextField(poly1);
		Polinom2 = new JTextField(poly2);
		RezultatAfisat = new JTextField();

		frame.add(Polinom1);
		frame.add(Polinom2);
		frame.add(RezultatAfisat);

		frame.setBounds(200, 100, 800, 800);
		titlu.setBounds(50, 10, 650, 50);

		Polinom1.setBounds(300, 185, 200, 30);
		Polinom1.setEditable(true);
		Polinom2.setBounds(300, 235, 200, 30);
		Polinom2.setEditable(true);
		adunare.setBounds(200, 300, 100, 50);
		scadere.setBounds(440, 300, 100, 50);
		inmultire.setBounds(200, 400, 100, 50);
		derivare.setBounds(200, 500, 100, 50);
		integrare.setBounds(440, 400, 100, 50);
		reset.setBounds(440, 500, 100, 50);
		Rezultat.setBounds(200, 550, 100, 100);
		RezultatAfisat.setBounds(300, 585, 200, 30);
		RezultatAfisat.setEditable(false);

		adunare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == adunare) {
					ParsePolinom parse = new ParsePolinom();
					Operatii opManager = new Operatii();
					ArrayList<Monom> polynom1 = parse.parsePolinom(Polinom1.getText());
					for (Monom m : polynom1) {
						polinom1.addMonom(m);
					}

					ArrayList<Monom> polynom2 = parse.parsePolinom(Polinom2.getText());
					for (Monom m : polynom2) {
						polinom2.addMonom(m);
					}
					RezultatAfisat.setText(opManager.sumPolinom(polinom1, polinom2).toString());

				}
			}
		});
		scadere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == scadere) {
					ParsePolinom parse = new ParsePolinom();
					Operatii opManager = new Operatii();
					ArrayList<Monom> polynom1 = parse.parsePolinom(Polinom1.getText());
					for (Monom m : polynom1) {
						polinom1.addMonom(m);
					}

					ArrayList<Monom> polynom2 = parse.parsePolinom(Polinom2.getText());
					for (Monom m : polynom2) {
						polinom2.addMonom(m);
					}
					RezultatAfisat.setText(opManager.difPolinom(polinom1, polinom2).toString());

				}
			}
		});
		inmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == inmultire) {
					ParsePolinom parse = new ParsePolinom();
					Operatii opManager = new Operatii();
					ArrayList<Monom> polynom1 = parse.parsePolinom(Polinom1.getText());
					for (Monom m : polynom1) {
						polinom1.addMonom(m);
					}

					ArrayList<Monom> polynom2 = parse.parsePolinom(Polinom2.getText());
					for (Monom m : polynom2) {
						polinom2.addMonom(m);
					}
					RezultatAfisat.setText(opManager.mulPolinom(polinom1, polinom2).toString());

				}
			}
		});
		derivare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == derivare) {
					ParsePolinom parse = new ParsePolinom();
					Operatii opManager = new Operatii();
					ArrayList<Monom> polynom1 = parse.parsePolinom(Polinom1.getText());
					for (Monom m : polynom1) {
						polinom1.addMonom(m);
					}

					RezultatAfisat.setText(opManager.derivPolinom(polinom1).toString());

				}
			}
		});
		integrare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == integrare) {
					ParsePolinom parse = new ParsePolinom();
					Operatii opManager = new Operatii();
					ArrayList<Monom> polynom1 = parse.parsePolinom(Polinom1.getText());
					for (Monom m : polynom1) {
						polinom1.addMonom(m);
					}

					RezultatAfisat.setText(opManager.integrPolinom(polinom1).toString());

				}
			}
		});
		
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == reset) {
					RezultatAfisat.setText("");
					polinom1 = new Polinom();
					polinom2 = new Polinom();
				}
			}
		});
	}
}
